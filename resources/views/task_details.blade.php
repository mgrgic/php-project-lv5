@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="row panel-body">
                        <b>@lang('messages.details')</b>
                    </div>
                    @foreach($tasksData as $task)
                        <form method="post" action="confirmStudent">
                            <input type="hidden" name="task_id" value="{{$task->id}}">
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>@lang('messages.name')</th>
                                        <th>@lang('messages.description')</th>
                                        <th>@lang('messages.study_type')</th>
                                        <th>@lang('messages.student')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{$task->thesis_name}}</td>
                                        <td>{{$task->thesis_task}}</td>
                                        <td>{{$task->sutdy_programme_type}}</td>
                                        <td>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <select name="student">
                                                <optgroup>@lang('messages.chose')</optgroup>
                                                @foreach($students as $student)
                                                    <option>{{$student}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <button type="submit" class="btn btn-info">@lang('messages.confirm')</button>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection