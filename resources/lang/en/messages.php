<?php


return [
    'welcome1' => 'Welcome, ',
    'welcome2' => ' ,you are logged in.',
    'name' => 'Task name',
    'name_en' => 'Task (en)',
    'description' => 'Description',
    'study_type' => 'Study type',
    'graduate' => 'Graduate',
    'undergraduate' => 'Undergraduate',
    'study' => 'Study programme',
    'menu_lang_eng' => 'English language menu',
    'menu_lang_cro' => 'Croatian language menu',
    'admin_message' => ', you are admin! You can change student role.',
    'prof_message' => ', you are professor! You can accept one student and add new task.',
    'stud_message' => ', you are student! You can enroll to following tasks.',
    'professor' => 'Professor',
    'student' => 'Student',
    'button' => 'Confirm',
    'add_task' => 'Add task',
    'apply' => 'Apply',
    'chosen_one' => 'The chosen one:',
    'details' => "Details",
    'chose' => 'Chose one student:'
];
