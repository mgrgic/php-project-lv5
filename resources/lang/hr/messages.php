<?php

return [
    'welcome1' => 'Dobro došli, ',
    'welcome2' => ' ,uspješno ste se logirali.',
    'name' => 'Naziv rada',
    'name_en' => 'Naziv rada (en)',
    'description' => 'Zadatak rada',
    'study_type' => 'Tip studija',
    'graduate' => 'Diplomski',
    'undergraduate' => 'Preddiplomski',
    'study' => 'Stručni studij',
    'menu_lang_eng' => 'Izbornik na engleskom jeziku',
    'menu_lang_cro' => 'Izbornik na hrvatskom jeziku',
    'admin_message' => ', ti si admin! Možeš promijeniti ulogu korisnicima.',
    'prof_message' => ', ti si profesor! Možeš prihvatiti jednog studenta i dodati rad.',
    'stud_message' => ', ti si student! Možeš se prijaviti na sljedeće radove.',
    'professor' => 'Profesor',
    'student' => 'Student',
    'button' => 'Potvrdi',
    'add_task' => 'Dodaj rad',
    'apply' => 'Prijavi se',
    'chosen_one' => 'Odabrani student:',
    'details' => "Detalji",
    'chose' => 'Odaberi jednog od prijavljenih studenata:'
];