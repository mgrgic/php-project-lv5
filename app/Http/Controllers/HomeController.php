<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use App;
use Illuminate\Foundation\Application;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $loggedUser = DB::table('users')->where('email', Auth::user()->email)->first();
        App::setlocale($loggedUser->locale);
        $allUsers = DB::table('users')->get();
        $dataUsers = array();
        foreach ($allUsers as $user) {
            array_push($dataUsers, $user);
        }
        $allTasks = DB::table('tasks')->get();
        $dataTasks = array();
        foreach ($allTasks as $task) {
            array_push($dataTasks, $task);
        }
        if (Auth::user()->role == '/') {
            return view('role_selection');
        } else {
            return view('home', ['dataUsers' => $dataUsers, 'dataTasks' => $dataTasks]);
        }
    }

    public function changeLang()
    {
        $userId = Input::get('user_id');
        $locale = Input::get('locale');
        DB::table('users')->where('id', $userId)->update(['locale' => $locale]);
        return Redirect::to('/home');
    }


}
