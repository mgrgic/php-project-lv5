<?php
/**
 * Created by PhpStorm.
 * User: Spajdic
 * Date: 26.5.2019.
 * Time: 11:23
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use App;
use Illuminate\Foundation\Application;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function setUserRole()
    {
        if (Auth::check()) {
            $userId = Auth::user()->id;
            $userRole = Input::get('role');
            DB::table('users')->where('id', $userId)->update(['role' => $userRole]);

            return view('welcome');
        }
    }

    public function editUserRole(){

        $userId = Input::get('user_id');
        $role = Input::get('role');
        DB::table('users')->where('id', $userId)->update(['role' => $role]);

        return Redirect::to('/home');
    }

    public function login(){
        $user = Input::get('user');
        $taskId = Input::get('taskId');
        $studentslist = DB::table('tasks')->get()->where('id',$taskId)->pluck('students');
        $studentslistString = (string)$studentslist[0];
        if (Auth::user()->role == 'Student' && strpos($studentslistString, $user) !== true) {
            if($studentslistString == "")
                $studentslistString = $user;
            else{
                $studentslistString = $studentslistString . ", " . $user;
            }
        }
        DB::table('tasks')->where('id', $taskId)->update(['students' => $studentslistString]);
        return Redirect::to('/home');
    }

    public function confirmStudent(){
        $student = Input::get('student');
        $taskId = Input::get('task_id');
        $studentsInTask = DB::table('tasks')->get()->where('id',$taskId);
        DB::table('tasks')->where('id', $taskId)->update(['the_chosen_one' => $student]);
        return Redirect::to('/home');
    }
}