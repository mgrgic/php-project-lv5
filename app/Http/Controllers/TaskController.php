<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use App;
use Illuminate\Foundation\Application;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function openMenu(){
        $loggedUser = DB::table('users')->where('email', Auth::user()->email)->first();
        App::setlocale($loggedUser->locale);
        return view('add_task');
    }


    public function addThesis(){
        $thesis_name = Input::get('thesis_name');
        $thesis_name_eng = Input::get('thesis_name_eng');
        $thesis_task = Input::get('');
        $study_programme_type = Input::get('study_programme_type');
        $professor = Input::get('');
        DB::table('tasks')->insert(
            [
                'thesis_name' => $thesis_name,
                'thesis_name_eng' => $thesis_name_eng,
                'thesis_task' => $thesis_task,
                'study_programme_type' => $study_programme_type,
                'professor' => $professor
            ]
        );

        return redirect('/home');
    }

    public function confirmStudent(){
        $task_id = Input::get('taskId');
        $tasksData = DB::table('tasks')->get()->where('id', $task_id);

        foreach ($tasksData as $task) {
            $students = array();
            $appliedStudents = array();
            array_push($appliedStudents,$task->studenti);
            $appliedStudentsParts = explode(',', $appliedStudents[0]);

            foreach($appliedStudentsParts as $part){
                array_push($students, $part);
            }
        }

        return view('task_details', ['tasksData' => $tasksData, 'students' => $students]);
    }
}