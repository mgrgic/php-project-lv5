<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('/home', 'UserController@setUserRole');

Route::post('/editUser', 'UserController@editUserRole');

Route::get('/addThesis', 'TaskController@openMenu');

Route::post('/addThesis', 'TaskController@addThesis');

Route::post('/language', 'HomeController@changeLang');

Route::post('/login', 'UserController@login');

Route::get('/confirm', 'TaskController@confirmStudent');

Route::post('/confirm', 'UserController@confirmStudent');
